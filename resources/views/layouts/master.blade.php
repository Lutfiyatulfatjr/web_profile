<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Lumia Bootstrap Template - Index</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{{URL::asset('img/favicon.png')}}}" rel="icon">
  <link href="{{{URL::asset('img/apple-touch-icon.png')}}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{{URL::asset('vendor/bootstrap/css/bootstrap.min.css')}}}" rel="stylesheet">
  <link href="{{{URL::asset('vendor/bootstrap-icons/bootstrap-icons.css')}}}" rel="stylesheet">
  <link href="{{{URL::asset('vendor/boxicons/css/boxicons.min.css')}}}" rel="stylesheet">
  <link href="{{{URL::asset('vendor/glightbox/css/glightbox.min.css')}}}" rel="stylesheet">
  <link href="{{{URL::asset('vendor/swiper/swiper-bundle.min.css')}}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{{URL::asset('css/style.css')}}}" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Lumia - v4.1.0
  * Template URL: https://bootstrapmade.com/lumia-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
@include('frontend.template.header')
<main id="main">
@yield('content')
</main>
@include('frontend.template.footer')